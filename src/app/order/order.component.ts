import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import {Router} from "@angular/router";
import { Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  orders:any = [];
  complete_orders:any = [];
  msg:any = '';
  order_current_status:string = '';
  mySubscription: any;
  order_id:any = '';
  order_date:any = '';
  customer_name:any = '';
  customer_phone:any = '';
  delivery_charge:any = '';
  net_amount:any = '';
  item_description:any = '';
  tax_charge:any = '';
  payment_method:any = '';
  totItemPrice:number;

  constructor(public rest:RestService,private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
   }

  ngOnInit() {
    // Get Food Items
    this.rest.callPostApi('get-orders',{user_id:'H9Y5x0aXZu53Jj35HW2R',user_type:'service_provider',service_provider:'',status:'pending'}).subscribe((response) => {
      if(response.status=="true") {
        this.orders = response.data;
      }
    });

    this.rest.callPostApi('get-orders',{user_id:'H9Y5x0aXZu53Jj35HW2R',user_type:'service_provider',service_provider:'',status:'complete'}).subscribe((response) => {
      if(response.status=="true") {
        this.complete_orders = response.data;
      }
    });
  }

  changeStatus(order_status,order_id) {
    if(confirm("An status email will be forwarded to customer. Are you sure you want to change this order status?")) {

      if(order_status=='cancelled') {
      }
      else {
        var cancel_reason = '';
        this.rest.callPostApi('change-order-status',{order_id:order_id,order_status:order_status,cancel_reason:cancel_reason}).subscribe((response) => {
          this.msg = response.data;
          this.router.navigate(['/orders']);
        });
      }

    }
  }

  getOrderDetails(order_id) {
    this.order_id = '';
    this.order_date = '';
    this.customer_name = '';
    this.customer_phone = '';
    this.delivery_charge = '';
    this.net_amount = '';
    this.item_description = '';
    this.tax_charge = '';
    this.payment_method = '';

    this.rest.callPostApi('view-order',{item_id:order_id,type:'view'}).subscribe((response) => {
      if(response.status=='true') {
        var jdata = response.data;
        console.log(jdata);
        this.order_id = "#"+jdata.order_id;
        this.order_date = jdata.order_date;
        this.customer_name = jdata.billing_user_name;
        this.customer_phone = jdata.user_phone;
        this.delivery_charge = "₹ "+parseFloat(jdata.delivery_charges).toFixed(2);
        this.net_amount = "₹ "+parseFloat(jdata.net_price).toFixed(2);

        this.totItemPrice = 0;
        for(var i=0;i<jdata.items.length;i++) {
          this.item_description = this.item_description + "<tr><td><h6>"+jdata.items[i].item_name+"</h6><i>QTY: "+jdata.items[i].quantity+"</i><br><i>NOTE: "+jdata.items[i].item_note+"</i><br><i>RATING: "+jdata.items[i].rating+"</i></td><td><h6>"+jdata.items[i].total_price+"</h6></td></tr>";

          this.totItemPrice = this.totItemPrice+parseFloat(jdata.items[i].total_price);
        }

        this.tax_charge = "₹ "+this.totItemPrice*parseFloat(jdata.sale_tax)/100;
        this.payment_method = jdata.payment_method;
      }
      else {
        alert('Error getting document.');
      }
    });
  }

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }

}
