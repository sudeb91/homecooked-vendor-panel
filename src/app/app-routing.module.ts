import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { OrderComponent } from './order/order.component';
import { OrderSuccessComponent } from './order-success/order-success.component';
import { SidebarComponent } from './common/sidebar/sidebar.component';
import { RightbarComponent } from './common/rightbar/rightbar.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { FoodItemsComponent } from './food-items/food-items.component';
import { CustomersComponent } from './customers/customers.component';
import { ReviewsComponent } from './reviews/reviews.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path:'home', component:HomeComponent },
  { path:'orders', component:OrderComponent },
  { path:'order-success', component:OrderSuccessComponent },
  { path:'my-account', component:MyAccountComponent },
  { path:'food-items', component:FoodItemsComponent },
  { path:'customers', component:CustomersComponent },
  { path:'reviews', component:ReviewsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [HomeComponent,OrderComponent,HeaderComponent,FooterComponent,SidebarComponent,RightbarComponent,OrderSuccessComponent,FoodItemsComponent,CustomersComponent,ReviewsComponent,MyAccountComponent]
