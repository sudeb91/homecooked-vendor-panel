import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../authentication.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  currentUser: any;

  constructor(
      private authenticationService: AuthenticationService,
      private router: Router
  ) {
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  logout() {
    this.authenticationService.logout();
  }

  ngOnInit() {
  }

  addRoute(link) {
    this.router.navigate([link]);
  }

}
