import { Component, OnInit } from '@angular/core';
import { RestService } from '../../rest.service';

@Component({
	selector: 'app-rightbar',
	templateUrl: './rightbar.component.html',
	styleUrls: ['./rightbar.component.css']
})
export class RightbarComponent implements OnInit {

  followVendors:any = [];
  followingVendors:any = [];
  responseFollow:any = [];

  constructor(public rest:RestService) {}
  
	ngOnInit() {
    // Get Follow Vendors
    this.rest.callPostApi('get-follow-vendors',{}).subscribe((response) => {
      if(response.status=="true") {
        this.followVendors = response.data;
      }
    });

    let userId = 'H9Y5x0aXZu53Jj35HW2R';
    this.followingChefs(userId);
  }

  followChef(vendorId,vendorName,profImg) {
    // Get Follow Vendors
    this.rest.callPostApi('follow-vendor',{user_id:'H9Y5x0aXZu53Jj35HW2R',user_name:'Sudeb Mukherjee',follow_user_id:vendorId,follow_user_name:vendorName,follow_user_img:profImg}).subscribe((response) => {
      if(response.status=="true") {
        this.responseFollow = response.data;
      }
    });
  }

  followingChefs(userId) {
    // Get Follow Vendors
    this.rest.callPostApi('get-following-vendors',{user_id:userId}).subscribe((response) => {
      if(response.status=="true") {
        this.followingVendors = response.data;
      }
    });
  }

}